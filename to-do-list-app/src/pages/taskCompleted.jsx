import "../styles/taskCompleted.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

function TaskCompleted({ taskComplete }) {
  return (
    <section className="TaskCompleted">
      <h1>Tasks Completed</h1>
      {taskComplete.map((task, index) => {
        return (
          <div className="TaskCompleted__task" key={index}>
            <FontAwesomeIcon icon={faCheck} className="TaskCompleted__icon-check"/>
            <div className="TaskCompleted__text">
              <p className="TaskCompleted__name">{task.name}</p>
              <p className="TaskCompleted__category">{task.category}</p>
            </div>
          </div>
        );
      })}
    </section>
  );
}

export default TaskCompleted;
