import { useState } from "react";

import "../styles/AddTask.scss";

function AddTask({ addTask }) {
  const [newItem, setNewItem] = useState({
    name: '',
    category: '',
  });

  function handleAddItem(event) {
    const newValue = event.target.value;
    const inputName = event.target.name; 
    setNewItem({
      ...newItem,
      [inputName]: newValue,
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setNewItem({
      name: '',
      category: '',
    });
  };

  return (
    <section className="AddTask">
      <h1>Add Task</h1>
      <form className="AddTask__form" onSubmit={handleSubmit}>
        <fieldset>
          <label htmlFor="name">Name of the task</label>
          <input
            id="name"
            name="name"
            type="text"
            className="AddTask__input"
            value={newItem.name}
            onChange={handleAddItem}
          />
        </fieldset>

        <fieldset>
          <label htmlFor="category">Category of the task</label>
          <input
            id="category"
            name="category"
            type="text"
            className="AddTask__input"
            value={newItem.category}
            onChange={handleAddItem}
          />
        </fieldset>

        <button
          className="AddTask__btn"
          onClick={() => {
            addTask(newItem);
          }}
          disabled={newItem ? "" : "disabled"}
        >
          Add
        </button>
      </form>
    </section>
  );
}

export default AddTask;
