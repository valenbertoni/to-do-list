import "../styles/Favorites.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle, faHeart } from "@fortawesome/free-solid-svg-icons";

//  import {useState} from 'react';

function Favorites({ favTasks, taskFavCompleted}) {


  return (
    <section className="Favorites">
      <h1>Your favorites tasks</h1>
      {favTasks.map((task, index) => {
        return (
          <article className="Favorites__container" key={index}>
            <div className="Favorites__container-check-txt">
              <button
                className="Favorites__btn"
                onClick={() => {
                  taskFavCompleted(task);
                }}
              >
                <FontAwesomeIcon
                  icon={faCircle}
                  className="Favorites__icon-checkbox"
                />
              </button>
              <div className="Favorites__txt">
                <p className="Favorites__txt-name">{task.name}</p>
                <p className="Favorites__txt-category">{task.category}</p>
              </div>
            </div>

            <button className="Favorites__btn-heart">
              <FontAwesomeIcon icon={faHeart} className="Favorites__icon-fav" />
            </button>
          </article>
        );
      })}
    </section>
  );
}

export default Favorites;
