import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeart,
  faCircle,
} from "@fortawesome/free-solid-svg-icons";
// import { Link } from "react-router-dom";

import "../styles/Home.scss";

function Home({ itemsToComplete, taskCompleted, favList }) {
  //   hacer el map al array que recibo como parametro para pintarlo
  // console.log(itemsToComplete);

  return (
    <section className="Home">
      <h1>Your tasks</h1>

      {itemsToComplete.map((item, index) => {
        return (
          <article key={index} className="Home__item">
            <div className="Home__check-txt">
              <button
                onClick={() => {
                  taskCompleted(item);
                }}
              >
                <FontAwesomeIcon icon={faCircle} className="Home__checkbox" />
              </button>

              {/* <input
                className="Home__checkbox"
                type="checkbox"
                onChange={() => {
                  taskCompleted(item);
                }}
              /> */}
              <div className="Home__txt">
                <p className="Home__txt-name">{item.name}</p>
                <p className="Home__txt-category">{item.category}</p>
              </div>
            </div>
            <button
            className="Home__btn-heart"
              onClick={() => {
                favList(item);
              }}
            >
              <FontAwesomeIcon icon={faHeart} className="Btn__heart" />
            </button>
          </article>
        );
      })}
    </section>
  );
}

export default Home;
