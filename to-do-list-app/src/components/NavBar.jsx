import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faCheck,
  faPlus,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

import "../styles/NavBar.scss";

function NavBar() {
  return (
    <section className="NavBar">
      <div className="NavBar__home-done-fav">
        <Link to="/">
          <FontAwesomeIcon
            icon={faHome}
            className="NavBar__home NavBar__icon"
          />
        </Link>

        <Link to="/taskCompleted">
          <div className="NavBar__tasks-completed">
            <FontAwesomeIcon icon={faCheck} className="NavBar__check" />
            <p>Tasks completed</p>
          </div>
        </Link>

        <Link to="/favorites">
          <FontAwesomeIcon icon={faHeart} className="NavBar__heart" />
        </Link>
      </div>

      <Link to="/addTask">
        <FontAwesomeIcon icon={faPlus} className="NavBar__add NavBar__icon" />
      </Link>
    </section>
  );
}

export default NavBar;
