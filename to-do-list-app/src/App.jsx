import { Switch, Route } from "react-router-dom";
import { useState } from "react";

import NavBar from "./components/NavBar";
import Home from "./pages/home";
import AddTask from "./pages/addTask";
import TaskCompleted from "./pages/taskCompleted";
import Favorites from './pages/favorites';

import "./App.css";

function App() {
  const [itemsToComplete, setItemsToComplete] = useState([]);
  function addTask(newItem) {
    setItemsToComplete([...itemsToComplete, newItem]);
  }

  const [taskComplete, setTaskComplete] = useState([]);
  function taskCompleted(item) {
    setTaskComplete([...taskComplete, item]);
    const newList = itemsToComplete.filter((itemToDelete) => {
      return (
        item !== itemToDelete
        ) 
    })
    setItemsToComplete(newList);
  }
 
  
  const [favTasks, setFavTasks] = useState([])
  function favList(item) {
    setFavTasks([...favTasks, item]);
  }

  function taskFavCompleted(task) {
    setTaskComplete([...taskComplete, task]);
    const newFavList = favTasks.filter((itemToDelete) => {
      return (
        task !== itemToDelete
        ) 
    })
    setFavTasks(newFavList);
    
    const newToCompleteList = itemsToComplete.filter((itemToDelete)=>{
      return (
        task !== itemToDelete
      )
    })
    setItemsToComplete(newToCompleteList);
  }

  return (
    <div className="App">
      <NavBar />
      <Switch>
        <Route path="/" exact>
          <Home
            itemsToComplete={itemsToComplete}
            taskCompleted={taskCompleted}
            favList={favList}
          />
        </Route>
        <Route path="/favorites" exact>
          <Favorites favTasks={favTasks} taskFavCompleted={taskFavCompleted}/>
        </Route>

        <Route path="/addTask" exact>
          <AddTask addTask={addTask} />
        </Route>

        <Route path="/taskCompleted" exact>
          <TaskCompleted 
            taskComplete={taskComplete}
          />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
